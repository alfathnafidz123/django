from django.shortcuts import render
from .serializer import ProjectSerializer, TestSerializer
from .models import Project, Test
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework import status, generics
from rest_framework.parsers import MultiPartParser, FormParser
# Create your views here.

class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

class TestViewSet(viewsets.ModelViewSet):
    parser_classes = [MultiPartParser]
    queryset = Test.objects.all()
    serializer_class = TestSerializer

    def pre_save(self, obj):
        obj.file_html = self.request.FILES.get('file')
        obj.file_json = self.request.FILES.get('file')

