from django.contrib import admin
from .models import Project, Test
# Register your models here.


class TestInline(admin.StackedInline):
    model = Test
    
class ProjectAdmin(admin.ModelAdmin):
    inlines = [TestInline, ]

admin.site.register(Project, ProjectAdmin)




