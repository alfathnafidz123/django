from .models import Project, Test
from rest_framework import serializers


class TestSerializer(serializers.ModelSerializer):
    file_html = serializers.FileField(use_url=True, allow_null=True, required=False)
    file_json = serializers.FileField(use_url=True, allow_null=True, required=False)
    class Meta:
        model = Test
        fields = ['id','project', 'versi', 'id_commit', 'file_html', 'file_json', 'created_at']

class ProjectSerializer(serializers.ModelSerializer):
   #tests = TestSerializer(many=True, allow_null=True)
    class Meta:
        model = Project
        fields = ['id', 'nama', 'url', 'repo_url', 'created_at']
        #extra_kwargs = {
            # 'tests': {'required':False }
        # }
