from django.db import models


class Project(models.Model):
    nama = models.CharField(max_length=200)
    url = models.TextField(blank=True, max_length=200)
    repo_url = models.TextField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nama


class Test(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    versi = models.CharField(max_length=200)
    id_commit = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    file_html = models.FileField(blank=True, default='')
    file_json = models.FileField(blank=True, default='')
